package com.lizejun.applicationtestusb

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import java.util.*


class MainActivity : AppCompatActivity() {

   lateinit var mPermissionIntent:PendingIntent
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<Button>(R.id.button_next).setOnClickListener {
            val intent = Intent(this, MainActivity2::class.java)
            startActivity(intent)
        }

    }

    override fun onResume() {
        super.onResume()
        val usbFilter = IntentFilter()
        usbFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED)
        usbFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED)
        registerReceiver(mUsbReceiver, usbFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(mUsbReceiver)
    }


    val ACTION_DEVICE_PERMISSION = "com.android.usb.USB_PERMISSION"
    // 获取usb权限的通知
    private val mUsbPermissionReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            Log.e("lzj","BroadcastReceiver in\n")
            if (ACTION_DEVICE_PERMISSION == action) {
                synchronized(this) {
                    val device = intent.getParcelableExtra<UsbDevice>(UsbManager.EXTRA_DEVICE)
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (device != null) {
                            Log.e("lzj","usb EXTRA_PERMISSION_GRANTED")
                            Log.e("getDeviceList","\ndevice name: "+device.getDeviceName()+"\ndevice product name:"
                                    +device.getProductName()+"\nvendor id:"+device.getVendorId()+
                                    "\ndevice serial: "+device.getSerialNumber()+ "\nproduct ID: "+device.productId);
                       //开始自己的工作
                       doWork(device)

                        }else{

                        }
                    } else {
                        Log.e("lzj","usb EXTRA_PERMISSION_GRANTED null!!!")
                    }
                }
            }
        }
    }

    private fun doWork(device: UsbDevice) {
        val connectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
//        connectivityManager

    }

    //插入拔出的事件通知
    private val mUsbReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            Log.e("lzj","BroadcastReceiver in\n")
            if (UsbManager.ACTION_USB_DEVICE_ATTACHED == action) {
                Log.e("lzj","ACTION_USB_DEVICE_ATTACHED\n")
                listDevices()
            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED == action) {
                Log.e("lzj","ACTION_USB_DEVICE_DETACHED\n")
                listDevices()
            }
        }
    }

    fun listDevices(){
                val usbManager = getSystemService(Context.USB_SERVICE) as UsbManager
        val deviceList = usbManager.deviceList
        val deviceIterator: Iterator<UsbDevice> = deviceList.values.iterator()
        val usbDevices: MutableList<UsbDevice> = ArrayList()
        while (deviceIterator.hasNext()) {
            val device = deviceIterator.next()
            usbDevices.add(device)
            Log.e("getDeviceList","\ndevice name: "+device.getDeviceName()+"\ndevice product name:"
                    +device.getProductName()+"\nvendor id:"+device.getVendorId()+
                    "\ndevice serial: "+device.getSerialNumber()+ "\nproduct ID: "+device.productId);

            //先判断是否为自己的设备
            //注意：支持十进制和十六进制
            //比如：device.getProductId() == 0x04D2
            if( device.getVendorId() == 3034 && device.getProductId() == 33145 ) {
                if(usbManager.hasPermission(device)) {
                    Log.e("getDeviceList","获取到了权限, 可以通讯了")
                } else {
                    //获取设备后操作设备所需动态申请的权限
                    mPermissionIntent = PendingIntent.getBroadcast(this, 0, Intent(ACTION_DEVICE_PERMISSION), 0)
                    val permissionFilter = IntentFilter(ACTION_DEVICE_PERMISSION)
                    registerReceiver(mUsbPermissionReceiver, permissionFilter)
                    usbManager.requestPermission(device,mPermissionIntent);
                }
            }

        }
    }
}